	;Spielt die Daten,die vom Rom-Port kommen
	; mit timer-a-data=64 ab.
	;64 entspricht etwa 9600 Hz.

	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	output	f:\mag_utis\mag_samp.prg

timer_a_data	=	50
parallel_flag	=	0
ADDTA	=	$fa0000
ADSTR	=	$fb0000
SYNC	=	$ffff820a
SOUND_CHIP	=	$ffff8800
IER_A	=	$fffffa07
IER_B	=	$fffffa09
IPR_A	=	$fffffa0b
IPR_B	=	$fffffa0d
IMR_A	=	$fffffa13
IMR_B	=	$fffffa15
ISR_A	=	$fffffa0f
ISR_B	=	$fffffa11
TASTEN_STATUS	=	$fffffc00
TASTEN_EMPFANG	=	$fffffc02
;*********************************************
;*********************************************
;*********************************************
los
	bra	reserve
;*********************************************
play
	movem.l	d0-d3/a0-a3,-(sp)
	move	#$2700,sr
	bclr	#5,$fffffa0f.w
	lea	(SOUND_CHIP).w,a0
	lea	tabelle(pc),a1
	lea	512(a1),a2
	lea	512(a2),a3
	move.l	#$08000000,d0
	move.l	#$09000000,d1
	move.l	#$0a000000,d2

	move.l	#$0700ff00,(a0)
	moveq	#0,d3
	move.b	(ADDTA+1).l,d3
	add	d3,d3
	move	(a1,d3.l),d0
	move	(a2,d3.l),d1
	move	(a3,d3.l),d2
	movem.l	d0-d2,(a0)
	move	(ADSTR).l,d0
	movem.l	(sp)+,d0-d3/a0-a3
	rte	
tabelle	include	tab1.s
	copyright
reserve
	init	stack(pc)
	Cconws	info(pc)
	Supexec	switch_off(pc)
	Jdisint	#13
	Xbtimer	play(pc),#timer_a_data,#1,#0
	Jenabint	#13
	Ptermres	#0,#$100+reserve-los
switch_off
	move	(ADSTR).l,d0
	rts
info	dc.b	$d,$a,'MAG-SAMP, (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Die Musikausgabe wird gestartet.',$d,$a,0
