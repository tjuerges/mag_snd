	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	output	f:\mag_utis\mag_play.prg

	;Spielt ein Sample mit timer-a-data=64 ab.
	;64 entspricht etwa 9600 Hz.

timer_a_data	=	64
parallel_flag	=	0

;*********************************************
;*********************************************
;*********************************************
los
	bra	reserve
weiter
	Fread	load(pc),length(pc),handle(pc)
	Fclose	handle(pc)
	move.l	length(pc),rest
	move.l	#load,int_position
	Dosound	sounds(pc)
	Xbtimer	play_sample_interrupt(pc),#timer_a_data,#1,#0
	Jenabint	#13
	move.l	length(pc),d0
	add.l	#$100+load-los,d0
	Ptermres	#0,d0
;*********************************************
play_sample_interrupt
	movem.l	d0-d2/a0,-(sp)
	move	#$2700,sr
	move.l	#$0700ff00,$ffff8800.w
	subq.l	#1,rest
	bmi.s	.sound_ende
	move.l	int_position(pc),a0	;aktuelle position der sample daten
	clr.l	d0
	move.l	#$08000000,d0
	move.l	#$09000000,d1
	move.l	#$0a000000,d2
	move.b	(a0)+,d0	;daten holen
	add	d0,d0
	move.l	a0,int_position
	lea	tabelle(pc),a0
	move	(a0,d0),d1
	lea	512(a0),a0
	move	(a0,d0),d2
	lea	512(a0),a0
	move	(a0,d0),d0
	lea	$ffff8800.w,a0
	movem.l	d0-d2,(a0)
.int_ganz_fertig
	bclr	#5,$fffffa0f.w
	movem.l	(sp)+,d0-d2/a0
	rte	
.sound_ende
	move.l	length(pc),rest
	move.l	#load,int_position
	bra.s	.int_ganz_fertig
length	dc.l	0
rest	dc.l	0
int_position	dc.l	0
tabelle	include	tab1.s

sounds	dc.b	0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,%111111,8,0,9,0
	dc.b	10,0,11,0,12,0,13,0,$82,0
	copyright
	even
load
reserve
	Cconws	inp(pc)
	lea	name(pc),a0
	Cconrs	#78,(a0)
	addq.l	#2,a0
	tst.b	-1(a0)
	beq	get_file_in_memory
	Jdisint	#13
	moveq	#0,d0
	move.b	-1(a0),d0
	clr.b	(a0,d0.l)

	Fsfirst	#0,(a0)
	tst.l	d0
	bmi	get_file_in_memory

	Fgetdta 
	move.l	d0,a2
	move.l	$1a(a2),length
	move.l	$1a(a2),d0

	Fopen	#0,name+2(pc)
	move	d0,handle
	bmi	get_file_in_memory
	bra	weiter
get_file_in_memory
	Pterm0
handle	dc.w	0
name	dc.l	20
inp	dc.b	$d,$a,'MAG-PLAY, (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Namen des Samples eingeben:',$d,$a,0
