Begin:
  text
  lea       (a0),a4 
  lea       (a1),a5 
  moveq     #1,d7 
  move.w    #$80,d2 
  moveq     #7,d3 
  clr.w     d4
L0000:
  cmp.w     #8,d7 
  bhi       L0004 
L0001:
  clr.w     d6
L0002:
  addq.w    #1,d6 
  ftwotox.w d3,fp0
  fmove.w   d4,fp1
  fsgldiv.x fp0,fp1 
  fmove.w   fp1,d0
  swap      d3
  moveq     #7,d5 
  clr.w     d3
  clr.w     d1
L0003:
  move.w    d0,d1 
  lsr.w     #1,d1 
  sub.w     d1,d0 
  sub.w     d1,d0 
  add.w     d3,d3 
  add.w     d0,d3 
  move.w    d1,d0 
  dbf       d5,L0003
  move.w    d3,d0 
  swap      d3
  fmove.x   fp5,fp0 
  fmul.w    d0,fp0
  fsgldiv.w #$100,fp0 
  fsincos.x fp0,fp7:fp6 
  lea       (0.b,a0,d4.w*2),a2
  lea       (0.b,a1,d4.w*2),a3
  fmove.w   (0.b,a2,d2.w*2),fp0 
  fmove.w   (0.b,a3,d2.w*2),fp1 
  fsglmul.x fp7,fp0 
  fsglmul.x fp6,fp1 
  fadd.x    fp1,fp0 
  fmove.w   (0.b,a3,d2.w*2),fp1 
  fmove.w   (0.b,a2,d2.w*2),fp2 
  fsglmul.x fp7,fp1 
  fsglmul.x fp6,fp2 
  fsub.x    fp2,fp1 
  fmove.w   (a2),fp2
  fmove.x   fp2,fp3 
  fsub.x    fp0,fp2 
  fmove.w   fp2,(0.b,a2,d2.w*2) 
  fmove.w   (a3),fp2
  fmove.x   fp2,fp4 
  fsub.x    fp1,fp2 
  fmove.w   fp2,(0.b,a3,d2.w*4) 
  fadd.x    fp0,fp3 
  fmove.w   fp3,(a2)
  fadd.x    fp1,fp4 
  fmove.w   fp4,(a3)
  addq.w    #1,d4 
  cmp.w     d2,d6 
  bne       L0002 
  add.w     d2,d4 
  cmp.w     #$FF,d4 
  bcs       L0001 
  clr.w     d4
  subq.w    #1,d3 
  lsr.w     #1,d2 
  addq.w    #1,d7 
  bra       L0000 
L0004:
  move.w    #-1,d7
L0005:
  addq.w    #1,d7 
  move.w    d7,d6 
  moveq     #7,d4 
  clr.w     d3
  clr.w     d1
L0006:
  move.w    d6,d2 
  lsr.w     #1,d2 
  sub.w     d2,d6 
  sub.w     d2,d6 
  add.w     d3,d3 
  add.w     d6,d3 
  move.w    d2,d6 
  dbf       d4,L0006
  move.w    d3,d0 
  cmp.w     d7,d0 
  bls.s     L0007 
  move.w    (0.b,a4,d7.w*2),d2
  move.w    (0.b,a5,d7.w*2),d3
  move.w    (0.b,a4,d0.w*2),(0.b,a4,d7.w*2) 
  move.w    (0.b,a5,d0.w*2),(0.b,a5,d7.w*2) 
  move.w    d2,(0.b,a4,d0.w*2)
  move.w    d3,(0.b,a5,d0.w*2)
L0007:
  cmp.w     #$FF,d7 
  bne.s     L0005 
ZUEND:
  END 
