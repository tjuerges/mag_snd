	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	gem	;Vdi- & Aes-Macros einbinden.

TEST	equ	0
debug	equ	0
	ifne	debug
	opt	x+
	endc
	
put_all:	MACRO
	movem.l	d0-a6,-(sp)
	ENDM
get_all:	MACRO
	movem.l	(sp)+,d0-a6
	ENDM

;**********FLAGS***********
no_break_flag	equ	0	;Flag, damit erkannt wird, da� jetzt keine
no_break	equ	no_break_flag
		;Unterbrechung erw�nscht ist.
MC68020	equ	1	;Zeigt an, da� ein MC68020 (oder h�her) im Spiel ist.
darstellung_flag	equ	2	;640*400 Pixel Aufl�sung (=1).
noch_nicht_saved	equ	3	;Aktuelles Sample schon abgespeichert (=1)?
memory_dirty	equ	4	;Used-Flag: 0=Kein Sample, 1=Sample im Speicher
amiga_sample	equ	5	;Sample Typ: 0=Atari,1=Amiga
merge_flag	equ	6	;Zeigt an, da� das n�chste Sample angeh�ngt wurde.
		;werden soll.
;*******KONSTANTEN*********
init_frequenz	equ	47261
init_schwelle	equ	137
offset	equ	$80
CON	equ	2
GEM_OFF	equ	0
GEM_ON	equ	1
max_frequenz	equ	614400	;Durch Timer max. m�gliche Frequenz.
logn	equ	8	;log2(anzahl/2)
anzahl	equ	2<<logn	;Anzahl der Samples, die die Fourieranalyse betrachtet.
anzahlhalb	equ	anzahl/2	;F�r die FFT
update_off	equ	1	;WIND_UPDATE
update_on	equ	0	;''
mouse_on	equ	257	;Maus an/aus.
mouse_off	equ	256
crsr_off	equ	0	;Cursor an/aus.
crsr_on	equ	1
ADDTA	equ	$fa0000	;Adresse der Daten des Rom-Port-Samplers.
ADSTR	equ	$fb0000	;Adresse des Rom-Port-Samplers.
;*********************************************
	include	mag_snd.i	;GEM-Definitionen des RCS.
;*********************************************
;*********************************************
	text
	init	stack(pc)	;init: eigener Stack & Mshrink.
los_gehts:	;HIER F�NGTS WIRKLICH AN!!!
	init_gem	#RC	;Gem mitteilen: wir sind's.

	movem	intout(pc),d0-d1
	movem	d0-d1,max_x
	addq	#1,d0
	addq	#1,d1
	movem	d0-d1,max_w
	bsr	clip_on

	Getrez
	cmp	#2,d0
	beq.b	.rez_ist_ok
	moveq	#2,d0
	lea	only_hires(pc),a0
	bsr	form_alert
	bra	ende
.rez_ist_ok:
	bsr	busybee	;Bienen-Maus, denn es wird noch initialisiert.

	lea	flags(pc),a1	;Programminterne Flags setzen.
	clr.b	(a1)
	move.l	#'_CPU',d7
	bsr	find_cookie
	cmp.l	#20,d7
	blo.s	.kein_cookie_x20
	bset	#MC68020,(a1)
.kein_cookie_x20:
	bset	#darstellung_flag,(a1)
	
	bsr	init_resources	;Resource-File einlesen.
	tst.b	d0	;Hat es geklappt?
	beq.s	.rsrc_ok	;ja, dann zu .rsrc_ok.

	moveq	#1,d0
	lea	keine_rsrc_datei(pc),a0	;Bescheid geben,da� nun
	bsr	form_alert	;das Ende folgt.
	bra	ende

.rsrc_ok:	;Ab hier: allgemeine Setup-Aufgaben.
	clr.b	vdi_buffer
	lea	pfad(pc),a0
	move.l	#'\*.*',(a0)+
	clr.l	(a0)

	btst	#MC68020,flags(pc)
	beq.s	.no_bitrev_tab
	bsr	bitrev
.no_bitrev_tab:
	move.b	#init_schwelle,schwelle
	move.b	schwelle(pc),peak
	move.l	#init_frequenz,d0
	move.l	d0,frequenz
	move.l	d0,d5
	move.l	#max_frequenz,d2
	bsr	long_div
	move.b	d2,timer_a_data
	move.l	#8192,echo_length
	move.l	#$320032,new

	moveq	#-1,d0
	bsr	malloc
	sub.l	#256*1024,d0	;256kB Speicher freihalten. Man kann nie wissen...
	move.l	d0,maximum	;Maximale Sample-L�nge festhalten.
	bsr	malloc
	move.l	d0,mem_free
	btst	#0,d0
	beq.s	.weiter2
	addq.l	#1,d0
.weiter2:
	move.l	d0,root_start	;root-start ist der Start f�r alle Samples.
	bsr	loeschen	;Verwaltung initialisieren.

	bsr	menu_bar_on	;Men�leiste einschalten.
	lea	klick_off(pc),a6	;Kein Tastaturklick (sicherheitshalber).
	bsr	supexec
	bsr	dosound	;Soundchip initialisieren.
	bsr	arrow	;Maus-Pfeil an, Bienen sind out.
	bsr.s	main_menu	;Ab zum Hauptmenu.

ente:	;Hier endet (kein Absturz vorausgesetzt) das Programm immer.
	bsr	busybee	;Doch wieder die Biene anzeigen.
	bsr	dosound
	lea	klick_on(pc),a6	;Tastaturklick ggf. wieder einschalten.
	bsr	supexec
	bsr	menu_bar_off	;Menuleiste ausschalten.
	rsrc_free	menu_adresse(pc)	;Resource-Speicher freigeben.
	move.l	mem_free(pc),a0	;Sample-Speicher wieder freigeben.
	Mfree	(a0)
ende:
	bsr	clip_off
	exit_gem	;GEM mitteilen: das war's.
	Pterm0	;Tsch����������!!

main_menu:	;Hauptmenu...
	lea	message_puffer(pc),a0
	clr.l	(a0)
	clr.l	4(a0)
	clr.l	8(a0)
	clr.l	12(a0)
	evnt_multi	#MU_KEYBD!MU_MESAG,,,,,,,,,,,,,,,a0

	cmp	#MN_SELECTED,(a0)	;Wurde ein Menu-Eintrag angeklickt?
	bne.s	.key

	lea	jmp_title(pc),a1	;Menu-Eintrag herausfinden
	move	8(a0),d0	;und ggf. die entsprechende Funktion
.jmp:	;per Sprungtabelle ausw�hlen und anspringen.
	ext.l	d0
	lsl	#2,d0
	move.l	(a1,d0.l),a1
	jsr	(a1)
	bsr	menu_tnormal	;Menu-Titel wieder deSELECTen.
	bra.s	main_menu	;Und ab in die Endlosschleife.
.key:	;Hier gelangt das Programm hin, wenn eine Taste gedr�ckt wurde.
	lea	key_title(pc),a1	;Doch, Prozedur siehe oben.
	cmp	#K_ALT,int_out+8	;ALTERNATE-Taste gedr�ckt?
	bne.s	.kein_alt	;Falls nicht: vergi� es!
	move	int_out+10(pc),d0	;Per Scan-Code wird die Funktion
	;ausgew�hlt.
	lsr	#8,d0
	bra.s	.jmp
.kein_alt:
	tst	int_out+8
	bne.s	main_menu
	move	int_out+10(pc),d0
	lsr	#8,d0
	cmp	#SCAN_HELP,d0
	beq.s	.jmp
	bra	main_menu
;************************************
invertiere_save_titel:
	movem.l	d1/a0,-(sp)
	move.l	menu_adresse(pc),a0
	clr.l	d1
	btst	#memory_dirty,flags(pc)
	bne.s	.weiter
	moveq	#DISABLED,d1
.weiter:
	moveq	#DREHEN,d0
	bsr	objc_change
	moveq	#VOLUME,d0
	bsr	objc_change
	moveq	#MONITOR,d0
	bsr	objc_change
	moveq	#RMONITOR,d0
	bsr	objc_change
	moveq	#GRAFIK,d0
	bsr	objc_change
	moveq	#SAVE8,d0
	bsr	objc_change
	moveq	#SAVEAM,d0
	bsr	objc_change
	moveq	#SAVEAT,d0
	bsr	objc_change
	moveq	#MERGE,d0
	bsr	objc_change
	moveq	#LOESCHEN,d0
	bsr	objc_change
	moveq	#MAKEHALL,d0
	bsr	objc_change
	moveq	#PARALLEL,d0
	bsr	objc_change
	moveq	#EFILTER,d0
	bsr	objc_change
	moveq	#SEND,d0
	bsr	objc_change
	movem.l	(sp)+,d1/a0
	rts
;************************************
ueber_mag_sound:	;ohne Worte
	move.l	a0,-(sp)
	bsr	letemfly_change
	move.l	info_adresse(pc),a0
	moveq	#INFO1,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	move.l	info_adresse+4(pc),a0
	moveq	#INFO2,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	bsr	letemfly_change
	move.l	(sp)+,a0
	rts
;************************************
quit:
	bsr.s	quit_save
	bsr	menu_tnormal
	addq.l	#4,sp
	rts
;************************************
quit_save:
	move.l	a0,-(sp)
	btst	#noch_nicht_saved,flags(pc)
	beq.s	.weiter_quit_save
	lea	noch_nicht_abgespeichert(pc),a0
	moveq	#2,d0
	bsr	form_alert
	cmp	#FA_MIDDLE,int_out
	bne.s	.weiter_quit_save
	bsr.s	save_memory
.weiter_quit_save:
	move.l	(sp)+,a0
	rts
;************************************
save_memory:	;speichert den speicherbereich
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	bsr	save_file
.abbruch:
	rts
;************************************
loeschen:	;ohne worte
	bsr.s	quit_save
	bsr	busybee
	move.l	root_start(pc),next_start
	lea	flags(pc),a0
	bclr	#noch_nicht_saved,(a0)
	bclr	#memory_dirty,(a0)
	bclr	#amiga_sample,(a0)
	bclr	#merge_flag,(a0)
	clr	handle
	clr.l	length
	moveq	#-1,d0
	move.l	d0,int_position
	move.l	d0,rest
	lea	fft_tab(pc),a0
	lea	anzahl*2(a0),a0
	move.l	a0,next_im
	lea	anzahl*2(a0),a0
	move.l	a0,next_be
	lea	anzahl*2(a0),a0
	move.l	a0,old_be
	bsr	invertiere_save_titel
	clr.l	d0
	clr.l	d1
	clr.l	d2
	clr.l	d3
	clr.l	d4
	clr.l	d5
	clr.l	d6
	clr.l	d7
	sub.l	a0,a0
	sub.l	a1,a1
	sub.l	a2,a2
	sub.l	a3,a3
	sub.l	a4,a4
	sub.l	a5,a5
	sub.l	a6,a6
	bsr	arrow
	rts
;************************************
grafisch_darstellen:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	movem.l	d1-d7/a0,-(sp)

	bsr	menu_bar_off	;Men�leiste deaktivieren.
	bsr	reserve_screen	;Bildschirminhalt retten.
	bsr	clr_scn	;Bildschirm l�schen.
	move.l	root_start(pc),a0	;Musikdatenanfang.
	move.l	length(pc),d2	;L�nge der Musikdaten.
	move	max_w(pc),d5	;Breite des Bildschirms in Pixeln.
	ext.l	d5
	cmp.l	d5,d2	;Bildschirmbreite>Musikdatenanzahl?
	blo.s	.sample_zu_klein	;Ja, dann Sprung.

	bsr	long_div
	move.l	d2,d7	;Offset der Samplewerte.
	moveq	#1,d4	;x_offset=1
.zurueck:
	move	max_x(pc),d6	;Die Schleife wird max_x-1-mal durchlaufen.
	subq	#1,d6
	clr.l	d0	;x0=0
	moveq	#1,d2	;x1=1
	move	max_h(pc),d3
	lsr	#1,d3	;y1=max_h/2

	move	d3,d5
	sub	#offset,d5	;y_offset=max_h/2-127
.loop:
	move	d3,d1	;y0=old_y1
	clr.l	d3
	move.b	(a0),d3	;y1=samplewert
	add	d5,d3	;y1=samplewert+y_offset
	bsr	v_pline	;Male die Amplitude.
	add	d4,d0	;x0=x0+x_offset
	add	d4,d2	;x1=x1+x_offset
	add.l	d7,a0	;samplewert=old_samplewert+offset
	dbf	d6,.loop

	bsr	evnt_multi	;Auf ein Ereignis warten.
	bsr	free_screen	;Bildschirmspeicher wieder freigeben.
	bsr	menu_bar_on	;Men�leiste aktivieren.
	movem.l	(sp)+,d1-d7/a0
.abbruch:
	rts	;Und tsch�!

.sample_zu_klein:
	subq	#1,d2
	divu	d2,d5	;Breite / Anzahl der Punkte
	and.l	#$ffff,d5	;= Offset der X-Werte.
	move.l	d5,d4	;Nach d4.
	moveq	#1,d7	;Offset der Samplewerte.
	bra.s	.zurueck
************************************
volume:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	movem.l	d1/a0,-(sp)

	clr.l	d0
	lea	lauter(pc),a0
	bsr	form_alert
	move	int_out(pc),d1
	cmp	#FA_RIGHT,d1
	beq.s	.ende

	bsr	busybee

	bset	#noch_nicht_saved,flags

	move.l	root_start(pc),a0
	move.l	length(pc),d0

	subq	#FA_LEFT,d1
	beq.s	.lauter
.leiser:
	move.b	(a0),d1
	bpl.s	.positiv
	subq.b	#5,d1
	bra.s	.weiter
.positiv:
	addq.b	#5,d1

.weiter:
	move.b	d1,(a0)+
	subq.l	#1,d0
	bne.s	.leiser

.ende:
	bsr	arrow
	movem.l	(sp)+,d1/a0
.abbruch:
	rts

.lauter:
	move.b	(a0),d1
	bpl.s	._positiv
	addq.b	#5,d1
	bra.s	._weiter

._positiv:
	subq.b	#5,d1
._weiter:
	move.b	d1,(a0)+
	subq.l	#1,d0
	bne.s	.lauter

	bra.s	.ende
************************************
make_hall:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	put_all

	bsr	busybee

	bset	#noch_nicht_saved,flags
	move.l	root_start(pc),a6
	move.l	length(pc),d6	;L�nge der Daten.
	move.l	echo_length(pc),d0
	cmp.l	d6,d0
	bhs.s	.abbruch1
	lea	(a6),a0	;Alte Daten.
	lea	(a6,d0.l),a1	;Start der neuen Daten.
	lea	(a6,d6.l),a2	;Ende der Daten.
.loop:
	clr.l	d0
	clr.l	d1
	move.b	(a0)+,d0
	move.b	(a1),d1
	sub.b	#offset,d0
	sub.b	#offset,d1
	ext	d0
	ext	d1
	muls	new(pc),d0
	muls	old(pc),d1
	add.l	d0,d1
	divs	#100,d1
	add.b	#offset,d1
	move.b	d1,(a1)+
	cmp.l	a1,a2
	bhs.s	.loop
.abbruch1:
	bsr	arrow
	get_all
.abbruch:
	rts
************************************
frequenz_aendern:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	movem.l	d1/a0,-(sp)

	clr.l	d0
	lea	a_neue_frequenz(pc),a0
	bsr	form_alert
	move	int_out(pc),d1
	cmp	#FA_RIGHT,d1
	beq.s	.ende

	bsr	busybee

	bset	#noch_nicht_saved,flags

	move.l	root_start(pc),a0
	lea	(a0),a1
	move.l	length(pc),d0

	subq	#FA_LEFT,d1
	beq.s	.lauter
.halbe:
	move.b	(a0)+,d2
	ext	d2
	move.b	(a0),d1
	ext	d1
	add	d1,d2
	lsr	#1,d2
.weiter:
	move.b	d2,(a1)+
	subq.l	#1,d0
	bne.s	.halbe

.ende:
	bsr	arrow
	movem.l	(sp)+,d1/a0
.abbruch:
	rts

.lauter:
	bra.s	.ende
._weiter:
	move.b	d1,(a1)+
	subq.l	#1,d0
	bne.s	.lauter

	bra.s	.ende
************************************
drehen:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	movem.l	d1/a0-a1,-(sp)
	bsr	busybee
	bset	#noch_nicht_saved,flags

	move.l	root_start(pc),a0
	move.l	length(pc),d0
	lea	(a0,d0.l),a1

	btst	#0,d0
	beq.s	.glatt
	subq.l	#1,d0
.glatt:
	lsr.l	#1,d0
.loop:
	move.b	(a0),d1
	move.b	-(a1),(a0)+
	move.b	d1,(a1)
	subq.l	#1,d0
	bne.s	.loop

	bsr	arrow
	movem.l	(sp)+,d1/a0-a1
.abbruch:
	rts
************************************
load_iff_8svx:	;l�dt ein iff-file ein und konvertiert es
	bsr	quit_save
	bsr	load_file
	tst	d0
	bmi.s	.abbruch
	bsr	busybee
	bsr	get_iff_data
	tst	d0
	bmi.s	.fehler
	bset	#amiga_sample,flags
	bsr	convert
	bsr	get_data_address
	bsr	arrow
.abbruch:
	bsr	invertiere_save_titel
	rts
.fehler:
	bsr	arrow
	lea	fehler_iff_laden(pc),a0
	clr.l	d0
	bsr	form_alert
	bra.s	.abbruch
;************************************
merge:	;hier werden mehrere samples im speicher ge'merged'
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	bset	#merge_flag,flags
	bsr	load_file
	tst	d0
	bmi.s	.abbruch
	bsr	get_data_address
	bclr	#merge_flag,flags
	bset	#noch_nicht_saved,flags
.abbruch:
	rts
;************************************
load_atari:	;na, was ist's wohl???
	bsr	quit_save
	bclr	#amiga_sample,flags
	bra.s	load_raw_weiter

load_amiga:	;s.o.
	bsr	quit_save
	bset	#amiga_sample,flags
load_raw_weiter:
	bclr	#merge_flag,flags
	bsr	load_file
	tst	d0
	bmi.s	.abbruch
	btst	#amiga_sample,flags(pc)
	beq.s	.ist_schon_atari
	bsr	convert
.ist_schon_atari:
	clr.l	file_size
	bsr	get_data_address
.abbruch:
	bsr	invertiere_save_titel
	rts
;************************************
save_atari:	;speichert ein atari-sample
	btst	#amiga_sample,flags(pc)
	beq.s	.weiter
	bsr	convert
	bset	#noch_nicht_saved,flags
.weiter:
	bsr	save_file
.return:
	rts
 ;************************************
save_amiga:	;ebenso wie oben
	btst	#amiga_sample,flags(pc)
	bne.s	.weiter
	bsr	convert_back
	bset	#noch_nicht_saved,flags
.weiter:
	bsr	save_file
	bsr	convert
.return:
	rts
;************************************
save_iff_8svx:	;dto.
	movem.l	d1-d2/d5/d7/a0-a2,-(sp)
	bsr	busybee
	move.l	frequenz(pc),d5
	cmp.l	#$ffff,d5
	bhi	.iff_frequenz
	move	d5,iff_frequenz
	move.l	#iff_header_ende-form_length,d0
	add.l	length(pc),d0
	move.l	d0,form_length
	move.l	length(pc),body_length
	move.l	#save_it,a2
	bsr	arrow
	bsr	file_input
	tst	d0
	beq.s	.abbruch
	bsr	busybee
	btst	#amiga_sample,flags(pc)
	bne.s	.ist_amiga
	bsr	convert_back
.ist_amiga:
	clr.l	d0
	bsr	fcreate
	move	d0,handle
	bmi	file_error
	lea	iff_header(pc),a1
	move.l	#iff_header_ende-form_length+4,d0
	move	handle(pc),d1
	bsr	fwrite
	move.l	root_start(pc),a1
	move.l	length(pc),d0
	bsr	fwrite
	move.l	d0,d1
	bsr	fclose
	bsr	convert
	bclr	#noch_nicht_saved,flags
.abbruch:
	bsr	arrow
.ende:	movem.l	(sp)+,d1-d2/d5/d7/a0-a2
	rts
.iff_frequenz:
	bsr	arrow
	lea	a_iff_frequenz(pc),a0
	clr.l	d0
	bsr	form_alert
	bra.s	.ende
;************************************
daten_senden:
	rts
;************************************
daten_empfangen:
	rts
;************************************
get_data_address:	;die speicherverwaltung soll halbwegs dynamisch sein
	move.l	a1,-(sp)
	move.l	length(pc),d0 ;f�r merging festhalten,
	move.l	root_start(pc),a1
	add.l	d0,a1	;wo der n�chste musik-block
	move.l	a1,next_start	;starten wird
	move.l	(sp)+,a1
	rts
;************************************
dfilter:
	cmp.l	#anzahl,length
	blo	.ende

	bsr	busybee
	move	#20,unten
	move	#120,oben
	lea	fft_tab(pc),a0
	move.l	next_im(pc),a1
	move.l	root_start(pc),a2
	move.l	length(pc),d7
	divu	#anzahl,d7
	swap	d7
	clr.l	d6
	move	d7,d6	;Rest
	subq	#1,d6
	swap	d7	;Anzahl der 512-Byte-Bl�cke.
	subq	#1,d7

.loop:	move	#anzahl-1,d0
	lea	(a0),a3
	lea	(a1),a4
	lea	(a2),a5
.iloop:	clr	d2
	move.b	(a5)+,d2	;Format der Samplewerte: Byte->Word.
	sub.b	#offset,d2
	move	d2,(a3)+	;Sample->fft_tab
	clr	(a4)+	;Komplexen Bereich l�schen
	dbf	d0,.iloop

	bsr	filter

	move	#anzahl-1,d0
	lea	(a0),a3
	lea	(a2),a4
.bloop:	clr	d2
	move	(a3)+,d2
	lsr	#5,d2
	add.b	#offset,d2
	move.b	d2,(a4)+
	dbf	d0,.bloop
	lea	(a4),a2
	dbf	d7,.loop


	move	d6,d0
	lea	(a0),a3
	lea	(a1),a4
	lea	(a2),a5
.iloop2:
	clr	d2
	move.b	(a5)+,d2	;Format der Samplewerte: Byte->Word.
	sub.b	#offset,d2
	move	d2,(a3)+	;Sample->fft_tab
	clr	(a4)+	;Komplexen Bereich l�schen
	dbf	d0,.iloop2

	move	#anzahl-1,d0
	sub	d6,d0
.loop2:	clr	(a3)+
	clr	(a4)+
	dbf	d0,.loop2

	bsr	filter

	move	d6,d0
.bloop2:
	clr	d2
	move	(a0)+,d2
	lsr	#5,d2
	add.b	#offset,d2
	move.b	d2,(a2)+
	dbf	d0,.bloop2

	bsr	arrow
.ende:	rts
;************************************
sample:
	bsr	quit_save
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	move.l	root_start(pc),int_position

	put_all
	lea	super_sample(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem

	tst.l	scratch_length
	beq.s	.abbruch
	lea	flags(pc),a0
	bset	#memory_dirty,(a0)
	bset	#noch_nicht_saved,(a0)
	move.l	scratch_length(pc),length
	bsr	save_file
	bsr	get_data_address
.abbruch:
	bsr	invertiere_save_titel
	rts

super_sample:
	move.l	maximum(pc),d1
	clr.l	d4
	moveq	#5,d6
	move.b	schwelle(pc),d7
	move.l	int_position(pc),a0

	bsr	install_timer

	lea	(ADSTR).l,a3
	lea	(ADDTA+1).l,a4
	lea	(ipra).w,a5

	ifeq	debug
	move.l	#sample_int_ende,return_kbd
	endc
	bsr	my_interrupt_on
	move.b	(a3),d3
.warte_loop:
	btst	d6,(a5)
	beq.s	.warte_loop

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	sample_int_ende
	endc

	move.b	d4,(a5)
	move.b	(a4),d2
	move.b	(a3),d3
	cmp.b	d7,d2
	bls.s	.warte_loop
	bsr	farben

	bsr	clearbit
.sample_loop:
	btst	d6,(a5)	;meldet der int etwas
	beq.s	.sample_loop	;nein

	move.b	d4,(a5)	;interrupt l�schen
	move.b	(a4),(a0)+	;daten holen
	move.b	(a3),d2	;sampler neu starten

	subq.l	#1,d1	;buffer voll?
	bne.s	.sample_loop	;nein
	bra	sample_int_ende
;*********************************************
peak_suchen:	;Hier wird die 'lauteste Stelle' der eingelesenen Daten
	;innerhalb eines Zeitraumes gesucht.
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	move.b	schwelle(pc),peak
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	lea	peak_str(pc),a0
	bsr	v_string

	put_all
	lea	super_peak(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem

	move.l	peak_adresse(pc),a0
	lea	PEAKT*OBJECT(a0),a0
	move.l	ob_spec(a0),a0
	lea	(a0),a1
	moveq	#7,d0
.clr:	clr.b	(a1)+
	dbf	d0,.clr

	move.l	frequenz(pc),d5
	move.l	peak_time(pc),d2
	bsr	write_time

	move.l	peak_adresse(pc),a0
	move	#PEAKP,d0
	clr.l	d1
	move.b	peak(pc),d1
	sub.b	#offset,d1
	bsr	write_byte

	bsr	letemfly_change
	move.l	peak_adresse(pc),a0
	moveq	#DPEAK,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	bsr	letemfly_change
.abbruch:
	rts


super_peak:
	bsr	install_timer	;Meine Timerroutine installieren.

	clr.l	d0
	move.l	#$08000000,d1
	move.l	#$09000000,d2
	move.l	#$0a000000,d3
	clr.l	d4
	clr.l	d5
	clr.l	d6

	lea	tabelle(pc),a0
	lea	512(a0),a1
	lea	512(a1),a2

	lea	(ipra).w,a3


	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on

	lea	(ADSTR).l,a6	;Startregister f�r den ROM-Port-Sampler.
	lea	(ADDTA+1).l,a5	;Datenregister des s.o.
	move.b	(a5),d7	;Falls schon da, Sampler-Wert l�schen.

	bsr	clearbit

.peak_loop:	;Schleife...
	btst	#TIMER_A,(a3)	;Piept's beim Timer???
	beq.s	.peak_loop	;Nein, also warten.

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	endc

	clr.l	d0	;Alten Sample-Wert l�schen.
	move.b	(a5),d0	;Wert abholen.
	move.b	(a6),d7	;Sampler reset
	clr.b	(a3)	;Timer l�schen.

.auswerten:
	move	d0,d5
	add	d0,d0
	move	(a0,d0.l),d1
	move	(a1,d0.l),d2
	move	(a2,d0.l),d3
	movem.l	d1-d3,(giselect).w
	addq.l	#1,d4	;Z�hler, damit die Zeit festgestellt werden kann.
	cmp.b	peak(pc),d5	;Schwelle mit akt. Wert vergleichen.
	bls.s	.peak_loop	;Falls der akt. Wert<=Schwelle ist:
	;Vergi� es.
	bsr	farben
	move.b	d5,peak
	move.l	d4,peak_time
	bra.s	.peak_loop	;...und wieder von vorn!
;************************************
aussteuern:	;hier wird ausgesteuert
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	lea	aus_str(pc),a0
	bsr	v_string

	put_all
	lea	super_aussteuern(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
.abbruch:
	rts

super_aussteuern:
	bsr	logbase	;Bildschirmadresse holen.
	bsr	calc_screenoffset	;Offset der ersten m�glichen
	;Punkte zum Bildschirmstart im Speicher berechnen.
	bsr	install_timer	;Frequenzgeber installieren.

	ifeq	debug
	clr.l	d0
	endc
	clr.l	d1
	clr.l	d7

	lea	vdi_buffer(pc),a1	;Tastaturpuffer.
	;Hier kommen die gespeicherten Me�werte hinein.

	lea	(ipra).w,a2

	lea	y_save(pc),a3

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on

	lea	(ADSTR).l,a5
	lea	(ADDTA+1).l,a4
	move.b	(a5),d2

.aus_loop:	;Aussteuerungsschleife.
	btst	#TIMER_A,(a2)	;Piept 's beim Timer?
	beq.s	.aus_loop	;Nein, also warten.

	cmp.b	#SCAN_SPC,(a1)
	bne.s	.no_space
	bchg	#darstellung_flag,flags
.no_space:
	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	clr.l	d0
	endc

	clr.l	d1
	move.b	(a4),d1
	move.b	(a5),d2
	clr.b	(a2)

.auswerten:
	move	d1,d6
	add	d6,d6
	move.l	#$08000000,d3
	move.l	#$09000000,d2
	move.l	#$0a000000,d4
	lea	tabelle(pc),a0
	move	(a0,d6.l),d3
	lea	512(a0),a0
	move	(a0,d6.l),d2
	lea	512(a0),a0
	move	(a0,d6.l),d4
	movem.l	d2-d4,(giselect).w

.display:
	cmp	#638,d0	;save_sample_x==max_x-2
	bls.s	.weiter	;nein
	clr.l	d0	;und d0 auch
.warte_null:
	btst	#TIMER_A,(a2)	;Piept 's beim Timer?
	beq.s	.warte_null	;Nein, also warten.

	clr.l	d1
	move.b	(a4),d1
	move.b	(a5),d2
	clr.b	(a2)

	move	d1,d6
	add	d6,d6
	move.l	#$08000000,d3
	move.l	#$09000000,d2
	move.l	#$0a000000,d4
	lea	tabelle(pc),a0
	move	(a0,d6.l),d3
	lea	512(a0),a0
	move	(a0,d6.l),d2
	lea	512(a0),a0
	move	(a0,d6.l),d4
	movem.l	d2-d4,(giselect).w
	cmp.b	#offset,d1
	bne.s	.warte_null
.weiter:
	btst	#darstellung_flag,flags(pc)
	beq	.aus_loop

	clr.l	d5
	move.b	(a3,d0.l),d5
	move	d0,d2
	move	d0,d4
	lsr	#3,d2
	move	d2,d7
	and	#7,d4
	eor	#7,d4
	lsl	#4,d5
	move	d5,d6
	add	d6,d6
	add	d6,d6
	add	d6,d5
	add	d5,d2
	bclr	d4,(a6,d2.l)	;fertig und l�schen

	move.b	d1,(a3,d0.l)
	move	d7,d2	;Koordinaten errechnen.
	lsl	#4,d1
	move	d1,d6
	add	d1,d1
	add	d1,d1
	add	d6,d1
	add	d1,d2
	bset	d4,(a6,d2.l)	;Fertig und setzen.
	addq	#1,d0	;x+=1
	bra	.aus_loop	;...und wieder von vorn!
;*********************************************
fourier_analyse:
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	lea	fourier_str1(pc),a0
	bsr	v_string

	put_all
	lea	super_fourier(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
.abbruch:
	rts

super_fourier:
	bsr	logbase	;ausgabe der samplewerte auf den bildschirm
	bsr	calc_screenoffset

	bsr	install_timer	;Meine Timerroutine installieren.

	move.l	#fft,jmp_fft
	btst	#MC68020,flags(pc)
	beq.s	.x20_ok
	move.l	#x20_fft,jmp_fft
.x20_ok:
	move	#anzahl-1,d0
	clr.l	d1
	clr.l	d3
	clr.l	d7
	lea	vdi_buffer(pc),a1
	lea	(ipra).w,a2
	lea	fft_tab(pc),a3

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on

	lea	(ADSTR).l,a5
	lea	(ADDTA+1).l,a4
	move.b	(a5),d2

;*********TEST*********
	IFNE	TEST
	lea	sinus_bin(pc),a5
	lea	512(a5),a4
	ENDC
;*********TEST*********

;*********Timerabfrage**********
.aus_loop:
	btst	#TIMER_A,(a2)	;piept's beim timer???
	beq.s	.aus_loop	;nein, also warten

	ifne	debug
	move	d0,-(sp)
	Bconstat	#CON
	tst	d0
	beq.s	.weiter
	addq	#2,sp
	bra	play_int_ende
.weiter:
	move	(sp)+,d0
	endc

	clr	d1
;*********TEST*********
	IFNE	TEST
	ELSEIF
	move.b	(a4),d1	;Rom-Port-Sampler
	move.b	(a5),d2
	ENDC
	move.b	d3,(a2)	;Interrupt l�schen.
;*********TEST*********

;*********TEST*********
	IFNE	TEST
	move.b	(a5)+,d1
	cmp.l	a4,a5
	beq.s	.new
;*********TEST*********
	ELSEIF
	sub.b	#offset,d1
	ext	d1
	ENDC

.go_on:	clr	anzahl*2(a3)	;Komplexen Teil der Daten l�schen.
	move	d1,(a3)+
	dbf	d0,.aus_loop	;Schleife schon n-mal durchlaufen?

	move.l	jmp_fft(pc),a3
	bset	#no_break_flag,flags
	movem.l	d1-a2/a4-a6,-(sp)
	jsr	(a3)

	moveq	#(anzahl/32)-1,d0
	move	#$8000,d2
	move.l	next_be(pc),a0
	move.l	old_be(pc),a1
.draw_loop:
	clr.l	d1
	move	(a1),d1
	lsl	#4,d1
	move	d1,d3
	add	d3,d3
	add	d3,d3
	add	d3,d1
	not	d2	;Bitmaske invertieren.
	and	d2,(a6,d1.l)	;Punkt l�schen.
	not	d2	;Bitmaske nocheinmal invertieren.

	clr.l	d1
	move	(a0)+,d1
	lsr	#5,d1
	move	d1,(a1)+

	lsl	#4,d1
	move	d1,d3
	add	d3,d3
	add	d3,d3
	add	d3,d1
	or	d2,(a6,d1.l)

	ror	#1,d2
	bhi.s	.draw_loop
	addq	#2,a6
	dbf	d0,.draw_loop

.end:	movem.l	(sp)+,d1-a2/a4-a6
	bclr	#no_break_flag,flags
	lea	fft_tab(pc),a3
	move	#anzahl-1,d0
	bra	.aus_loop	;...und wieder von vorn!

	IFNE	TEST
;*********TEST*********
.new:
	lea	sinus_bin(pc),a5
	bra.s	.go_on
;*********TEST*********
	ENDC

shuffle:	;Hier wird ein Byte gespiegelt.
	clr	d2
	move	#anzahl/2,d5	;d5=anzahl/2
	moveq	#1,d6
	moveq.b	#logn,d4	;d4=log2(anzahl)
.shuff1:
	move	d0,d1
	and	d5,d1	;d1=d1&(anzahl/2)(=255)
	asr	d4,d1	;d1=d1>>7
	or	d1,d2	;d2=d2!d1
	asr	#1,d5	;d5=d5/2
	move	d0,d1	;d1=d0
	and	d6,d1	;d1=d1&d6
	asl	d4,d1	;d1=d1<<7
	or	d1,d2	;d2=d2!d1
	;asl	#1,d6
	add	d6,d6	;d6=d6*2
	subq.b	#2,d4	;d4=d4-2
	bpl.s	.shuff1	;sgn(d4)==1? =>.shuff1
	rts

;************* FFT 68000 *********************
fft:	bset	#no_break_flag,flags
	lea	fft_tab(pc),a4
	lea	next_im(pc),a5

transform:
	lea	(a4),a0
	lea	(a5),a1
	moveq	#1,d0
.ford0:
	bsr.s	shuffle
	cmp	d0,d2	;d2>d0
.wird:
	bls.s	.notausch	;Nein, dann .notausch.
	move	d0,d1	;Doch. d1=d0
	add	d1,d1	;d1=d1*2
	add	d2,d2	;d2=d2*2
	move	(a0,d1),d3	;d3=(fft_tab+d1)
	move	(a0,d2),(a0,d1)	;(fft_tab+d1)=(fft_tab+d2)
	move	d3,(a0,d2)	;(fft_tab+d2)=d3
	move	(a1,d1),d3	;d3=(next_im+d1)
	move	(a1,d2),(a1,d1)	;(next_im+d2)=(next_im+d1)
	move	d3,(a1,d2)	;(next_im+d2)=d3
.notausch:
	addq	#1,d0	;d0=d0+1
	cmp	#anzahl,d0	;d0<anzahl-1
	bcs.s	.ford0
ffttrans:
	lea	(a4),a0
	lea	(a5),a1
	lea	sinus(pc),a2
	moveq	#1,d0
.while:
	cmp	#anzahl,d0
	bcc	.trend
	clr	d1
.form:	move	#804,d5
	muls	d1,d5
	divs	d0,d5
	clr	d4
	cmp	#402,d5
	bcs.s	.intab
	sub	#804,d5
	neg	d5
	moveq	#1,d4
.intab:	add	d5,d5
	move	(a2,d5),wi
	;cmp	#1,vorrueck	;Bei vorrueck=1 ist es eine Analyse,
	;beq.s	.minsin	;deshalb kann man es hier weglassen.
	;neg	wi
.minsin:
	sub	#804,d5
	neg	d5
	move	(a2,d5),wr
	btst	#0,d4
	beq.s	.intab1
	neg	wr
.intab1:
	move	d1,d2
.repeat1:
	move	d2,d3
	add	d0,d3
	add	d3,d3
	move	(a0,d3),d4
	move	(a1,d3),d5
	move	d4,d6
	muls	wr(pc),d6
	move	d5,d7
	muls	wi(pc),d7
	sub.l	d7,d6
	asr.l	#8,d6
	move	d6,tr
	muls	wr(pc),d5
	muls	wi(pc),d4
	add.l	d4,d5
	asr.l	#8,d5
	move	d5,ti
	add	d2,d2
	move	(a0,d2),d4
	move	(a1,d2),d5
	move	d4,d6
	move	d5,d7
	sub	tr(pc),d6
	sub	ti(pc),d7
	move	d6,(a0,d3)
	move	d7,(a1,d3)
	add	tr(pc),d4
	add	ti(pc),d5
	move	d4,(a0,d2)
	move	d5,(a1,d2)
	asr	#1,d2
	add	d0,d2
	add	d0,d2
	cmp	#anzahl,d2
	bcs.s	.repeat1
	addq	#1,d1
	cmp	d1,d0
	bhi	.form
	add	d0,d0
	bra	.while
.trend:
betrag:	lea	(a4),a0
	lea	(a5),a1
	move.l	next_be(pc),a2
	clr	d0
.nextdate:
	clr.l	d1
	move	(a0,d0),d1
	muls	d1,d1
	clr.l	d2
	move	(a1,d0),d2
	muls	d2,d2
	add.l	d2,d1
	move.l	d1,d3
	beq.s	.istnull
	add.l	#$10000,d3
	asr.l	#1,d3
.repxly:
	move.l	d3,d2
	move.l	d1,d4
	asr.l	#8,d3
	divu	d3,d4
	and.l	#$ffff,d4
	asl.l	#8,d4
	add.l	d2,d4
	asr.l	#1,d4
	move.l	d4,d3
	cmp.l	d2,d3
	bcs.s	.repxly
	asr.l	#8,d3
.istnull:
	move	d3,(a2,d0)
	addq	#2,d0
	cmp	#anzahl,d0
	bcs.s	.nextdate

	rts
;************* FFT 68020 *********************
filter:	movem.l	d0-a6,-(sp)
	lea	(a0),a4
	lea	(a1),a5

	sf	vor
	bsr	_x20_fft
	bsr.s	_x20_clear_koeff
	st	vor
	bsr	_x20_fft

	movem.l	(sp)+,d0-a6
	rts

x20_fft:
	lea	fft_tab(pc),a4
	move.l	next_im(pc),a5
	sf	vor
	bsr	_x20_fft
	bsr.s	_x20_betrag

	rts

_x20_clear_koeff:
	move	unten(pc),d0
	move	oben(pc),d1
	lea	(a4),a0
	lea	(a5),a1
.fclr:
	move	d0,d2
	neg	d2
	add	#(anzahl*2)-2,d2
	clr	(a0,d0)
	clr	(a1,d0)
	clr	(a0,d2)
	clr	(a1,d2)
	addq	#2,d0
	cmp	d1,d0
	blo.b	.fclr
	rts

.norm:	lea	(a4),a0
	move	next_be(pc),a5
	move	#anzahl,d1
.n_loop:
	clr.l	d0
	move	(a0),d0
	divu	d1,d0
	move	d0,(a0)+
	cmp.l	a0,a5
	blo.b	.n_loop
	rts

* BETRAG bildet aus den Fourierkomponenten die Quadratwurzeln.
* Man erh�lt also das gew�nschte Amplitudenspektrum der Frequenzen.
_x20_betrag:
	lea	(a4),a0
	lea	(a5),a1
	move.l	next_be(pc),a2
	clr	d0
.nextdate:
	clr.l	d1
	move	(a0,d0),d1
	muls	d1,d1
	clr.l	d2
	move	(a1,d0),d2
	muls	d2,d2
	add.l	d2,d1
	move.l	d1,d3
	beq.b	.istnull
	add.l	#$10000,d3
	asr.l	#1,d3
.repxly:
	move.l	d3,d2
	move.l	d1,d4
	asr.l	#8,d3
	divu	d3,d4
	and.l	#$ffff,d4
	asl.l	#8,d4
	add.l	d2,d4
	asr.l	#1,d4
	move.l	d4,d3
	cmp.l	d2,d3
	blo.b	.repxly
	asr.l	#8,d3
.istnull:
	move	d3,(a2,d0)
	addq	#2,d0
	cmp	#anzahl,d0
	blo.b	.nextdate
	rts

_x20_mixup:	lea	(a4),a0
	lea	(a5),a1
	lea	bitrev_t(pc),a2
	moveq	#1,d0
.ford0:	move	(a2,d0.w*2),d2
	cmp	d0,d2	;d2>d0
	bls.b	.notausch	;Nein, dann .notausch.
	move	(a0,d0.w*2),d3	;d3=(fft_tab+d1)
	move	(a1,d0.w*2),d4	;d4=(next_im+d1)
	move	(a0,d2.w*2),(a0,d0.w*2)	;(fft_tab+d1)=(fft_tab+d2)
	move	(a1,d2.w*2),(a1,d0.w*2)	;(next_im+d2)=(next_im+d1)
	move	d3,(a0,d2.w*2)	;(fft_tab+d2)=d3
	move	d4,(a1,d2.w*2)	;(next_im+d2)=d4
.notausch:
	addq	#1,d0	;d0=d0+1
	cmp	#anzahl,d0	;d0<anzahl?
	blo.b	.ford0	;Ja, also weiter in der Schleife.
	rts

_x20_fft:
	bsr.b	_x20_mixup
	lea	(a4),a0
	lea	(a5),a1
	lea	sinus(pc),a2
	moveq	#1,d0
.while:	cmp	#anzahl,d0
	bhs	.trend
	clr	d1
.form:	move	#804,d5
	muls	d1,d5
	divs	d0,d5
	clr	d4
	cmp	#402,d5
	blo.b	.intab
	sub	#804,d5
	neg	d5
	moveq	#1,d4
.intab:	move	(a2,d5.w*2),wi
	tst.b	vor(pc)
	beq.b	.no_back
	neg	wi
.no_back:
	sub	#402,d5
	neg	d5
	move	(a2,d5.w*2),wr
	btst	#0,d4
	beq.b	.intab1
	neg	wr
.intab1:
	move	d1,d2
.repeat1:
	move	d2,d3
	add	d0,d3
	move	(a0,d3.w*2),d4
	move	(a1,d3.w*2),d5
	move	d4,d6
	muls	wr(pc),d6
	move	d5,d7
	muls	wi(pc),d7
	sub.l	d7,d6
	asr.l	#8,d6
	move	d6,tr
	muls	wr(pc),d5
	muls	wi(pc),d4
	add.l	d4,d5
	asr.l	#8,d5
	move	d5,ti
	move	(a0,d2.w*2),d4
	move	(a1,d2.w*2),d5
	move	d4,d6
	move	d5,d7
	sub	tr(pc),d6
	sub	ti(pc),d7
	move	d6,(a0,d3.w*2)
	move	d7,(a1,d3.w*2)
	add	tr(pc),d4
	add	ti(pc),d5
	move	d4,(a0,d2.w*2)
	move	d5,(a1,d2.w*2)
	add	d0,d2
	add	d0,d2
	cmp	#anzahl,d2
	blo.b	.repeat1
	addq	#1,d1
	cmp	d1,d0
	bhi	.form
	add	d0,d0
	bra	.while
.trend:	rts

* Bitspiegelung f�r 9 Bit.
* IN: D0=Byte
* OUT: D1=gespiegeltes Byte
* 10.4.93
bitrev:
	lea	bitrev_t(pc),a0
	move	#anzahl-1,d6
.loop:	move	(a0,d6.w*2),d0
	clr.l	d4
	clr.l	d3
	bfextu	d0{31:1},d3
	bfins	d3,d4{23:1}
	bfextu	d0{30:1},d3
	bfins	d3,d4{24:1}
	bfextu	d0{29:1},d3
	bfins	d3,d4{25:1}
	bfextu	d0{28:1},d3
	bfins	d3,d4{26:1}
	bfextu	d0{27:1},d3
	bfins	d3,d4{27:1}
	bfextu	d0{26:1},d3
	bfins	d3,d4{28:1}
	bfextu	d0{25:1},d3
	bfins	d3,d4{29:1}
	bfextu	d0{24:1},d3
	bfins	d3,d4{30:1}
	bfextu	d0{23:1},d3
	bfins	d3,d4{31:1}
	move	d4,(a0,d6.w*2)

	dbf	d6,.loop
	rts

clearbit:
	btst	#MC68020,flags(pc)
	beq.b	.s_f_b
	move.l	d6,-(sp)
	movec	cacr,d6
	bset	#3,d6	;Clear cache memory.
	movec	d6,cacr
	move.l	(sp)+,d6
.s_f_b:	rts
;*********************************************
play_monitor:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	put_all
	lea	super_play(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
.abbruch:
	rts

super_play:
	bsr	install_timer
	move.l	root_start(pc),a0
	lea	(ipra).w,a2
	lea	(giselect).w,a3
	lea	tabelle(pc),a4
	lea	512(a4),a5
	lea	512(a5),a6
	ifeq	debug
	move.l	#$08000000,d0
	endc
	move.l	#$09000000,d1
	move.l	#$0a000000,d2
	clr.l	d5
	moveq	#5,d6
	move.l	length(pc),d7

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on

	bsr	clearbit
.play_n_interrupt:
	btst	d6,(a2)
	beq.s	.play_n_interrupt

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	move.l	#$08000000,d0
	endc

	clr.l	d3
	move.b	(a0)+,d3	;daten holen
	add	d3,d3
	move	(a4,d3.l),d0
	move	(a5,d3.l),d1
	move	(a6,d3.l),d2
	movem.l	d0-d2,(a3)
	clr.b	(a2)	;interrupt l�schen
	subq.l	#1,d7
	bne.s	.play_n_interrupt

	move.l	length(pc),d7
	move.l	root_start(pc),a0
	bra.s	.play_n_interrupt
;************************************
;*********************************************
play_monitor_rueckw:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	put_all
	lea	.super_play_rueckw(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
.abbruch:
	rts

.super_play_rueckw:
	bsr	install_timer
	move.l	root_start(pc),a0
	add.l	length(pc),a0
	addq	#1,a0
	lea	(ipra).w,a2
	lea	(giselect).w,a3
	lea	tabelle(pc),a4
	lea	512(a4),a5
	lea	512(a5),a6
	ifeq	debug
	move.l	#$08000000,d0
	endc
	move.l	#$09000000,d1
	move.l	#$0a000000,d2
	clr.l	d5
	moveq	#5,d6
	move.l	length(pc),d7

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on

	bsr	clearbit
.play_n_interrupt:
	btst	d6,(a2)
	beq.s	.play_n_interrupt

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	move.l	#$08000000,d0
	endc

	clr.l	d3
	move.b	-(a0),d3	;daten holen
	add	d3,d3
	move	(a4,d3.l),d0
	move	(a5,d3.l),d1
	move	(a6,d3.l),d2
	movem.l	d0-d2,(a3)
	clr.b	(a2)	;interrupt l�schen
	subq.l	#1,d7
	bne.s	.play_n_interrupt

	move.l	length(pc),d7
	move.l	root_start(pc),a0
	add.l	d7,a0
	addq	#1,a0
	bra.s	.play_n_interrupt
;************************************
play_parallel:
	btst	#memory_dirty,flags(pc)
	beq.s	.abbruch
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	put_all
	lea	.super_play(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
.abbruch:
	rts

.super_play:
	bsr	install_timer
	move.l	root_start(pc),a0
	lea	(a0),a1
	lea	(ipra).w,a2
	lea	(giselect).w,a3
	lea	(giwrite).w,a4
	moveq	#5,d1
	move.l	length(pc),d2
	move.l	d2,d3

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on
	move.l	#$0700ff00,(a3)
	bsr	clearbit
.play_n_interrupt:
	btst	d1,(a2)
	beq.s	.play_n_interrupt

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	endc

	move.b	#$0f,(a3)	;Parallel-Port-Sampler
	move.b	(a0)+,(a4)	;Daten holen+schreiben.
	clr.b	(a2)	;Interrupt-in-service-Register l�schen.
	subq.l	#1,d2
	bne.s	.play_n_interrupt

	move.l	d3,d2
	lea	(a1),a0
	bra.s	.play_n_interrupt
;************************************
play_hall_centronics:
	bsr	quit_save
	bsr	zeige_abbruch_box
	beq.s	.abbruch
	moveq	#GEM_OFF,d0
	bsr	ciao_gem

	put_all
	lea	.super_play(pc),a6
	bsr	supexec
	get_all

	moveq	#GEM_ON,d0
	bsr	ciao_gem
	clr.l	length
	bclr	#noch_nicht_saved,flags
	bclr	#memory_dirty,flags
	bsr	invertiere_save_titel
.abbruch:
	rts

.super_play:
	bsr	install_timer
	lea	(ADSTR).l,a0
	lea	(ADDTA+1).l,a1
	lea	(ipra).w,a2
	lea	(giselect).w,a3
	lea	(giwrite).w,a4
	move.l	root_start(pc),a6
	lea	(a6),a5	;a6=Start des Speichers, der benutzt wird.

	moveq	#5,d1	;mfp-Bit #5=Timer A.
	move.l	echo_length(pc),d7
	move.l	d7,d4
.loop_echo:
	cmp.l	maximum(pc),d4
	bls.s	.ok
	subq.l	#8,d4
	bra.s	.loop_echo
.ok:
	bsr.s	.clear_echo

	ifeq	debug
	move.l	#play_int_ende,return_kbd
	endc
	bsr	my_interrupt_on
	move.l	#$0700ff00,(a3)
	bsr	clearbit
.play_n_interrupt:
	btst	d1,(a2)
	beq.s	.play_n_interrupt

	ifne	debug
	Bconstat	#CON
	tst	d0
	bne	play_int_ende
	endc

	clr.l	d5
	clr.l	d6
	move.b	(a1),d5	;d5=Brandneuer Wert.
	move.b	(a0),d0	;Sampler neu starten.
	move.b	(a5),d6	;d6=Alter Wert.
	sub.b	#offset,d5
	sub.b	#offset,d6
	ext	d5
	ext	d6
	muls	new(pc),d5
	muls	old(pc),d6
	add.l	d6,d5
	divs	#100,d5
	add.b	#offset,d5

	move.b	d5,(a5)+
	move.b	#$0f,(a3)	;Parallel-Port-Sampler
	move.b	d5,(a4)	;Daten schreiben.
	clr.b	(a2)	;Timer-Interrupt l�schen.
	subq.l	#1,d4
	bne.s	.play_n_interrupt

	move.l	d7,d4	;d4=echo_length;
	move.l	a6,a5	;a5=echo_start;
	bra.s	.play_n_interrupt

.clear_echo:
	move.l	a6,-(sp)
	move.l	d7,d6
	lsr.l	#2,d6
.loop:
	clr.l	(a6)+
	subq.l	#1,d6
	bne.s	.loop
	btst	#1,d7
	beq.s	.no_word
	clr	(a6)+
.no_word:
	btst	#0,d7
	beq.s	.no_byte
	clr.b	(a6)
.no_byte:
	move.l	(sp)+,a6
	rts
;************************************
get_iff_data:	;IFF-Daten feststellen
	movem.l	d1-d2/d5/a0-a1,-(sp)
	move.l	root_start(pc),a0	;Samplel�nge und Samplefrequenz.
	lea	(a0),a1
	cmp.l	#'FORM',(a0)+	;Jedes IFF-File beginnt mit 'FORM'.
	bne.s	fehler_iff
	move.l	(a0)+,d0	;Gesamte FORM-L�nge in d0.
	move.l	d0,d1
.loop:
	cmp.l	#'8SVX',(a0)+
	beq.s	.ist_8svx
	subq.l	#4,d0
	bne.s	.loop
	bra.s	fehler_iff
.ist_8svx:
	lea	(a1),a0
	move.l	d1,d0
.loop2:
	cmp.l	#'VHDR',(a0)+
	beq.s	vhdr_gefunden
	subq.l	#4,d0
	bne.s	.loop2
	bra.s	fehler_iff
vhdr_gefunden:
	addq.l	#8,a0
	addq.l	#8,a0
	clr.l	d5
	move	(a0)+,d5	;Frequenz gefunden.
	move.l	d5,frequenz
	lea	(a1),a0
	move.l	d1,d0
.loop:
	cmp.l	#'BODY',(a0)+
	beq.s	body_gefunden
	subq.l	#4,d0
	bne.s	.loop
	bra.s	fehler_iff
body_gefunden:
	move.l	(a0)+,length	;L�nge des Samples.
	move.l	#max_frequenz,d2
	bsr	long_div
	move.b	d2,timer_a_data
	move.l	length(pc),d2
	move.l	root_start(pc),a1
.loop:
	move.b	(a0)+,(a1)+	;Werte umkopieren.
	subq.l	#1,d2
	bne.s	.loop
	moveq	#0,d0
ende_iff:
	movem.l	(sp)+,d1-d2/d5/a0-a1
	rts
fehler_iff:
	bclr	#memory_dirty,flags
	bclr	#noch_nicht_saved,flags
	moveq	#-1,d0
	bra.s	ende_iff
;************************************
convert:	;konvertiert amiga-sample-daten in atari-sample-daten
	move.l	a0,-(sp)
	move.l	root_start(pc),a0
	move.l	length(pc),d0
.loop:
	add.b	#offset,(a0)+
	subq.l	#1,d0
	bne.s	.loop
	move.l	(sp)+,a0
	bclr	#amiga_sample,flags
	rts
;*************************************
convert_back:	;konvertiert atari-sample-daten in amiga-sample-daten
	move.l	a0,-(sp)
	move.l	root_start(pc),a0
	move.l	length(pc),d0
.loop:
	sub.b	#offset,(a0)+
	subq.l	#1,d0
	bne.s	.loop
	move.l	(sp)+,a0
	bset	#amiga_sample,flags
	rts
;*************************************
werte_ascii_aus:
	move.l	d3,-(sp)
	subq	#1,d0
	clr.l	d1
.loop:
	mulu	#10,d1
	clr.l	d3
	move.b	(a0)+,d3
	sub.b	#'0',d3
	add.b	d3,d1
	dbf	d0,.loop
	move.l	(sp)+,d3
	rts
;************************************
get_timer_data:
	move.b	timer_a_data(pc),d5	;frequenz holen f�r die verz�gerung
	moveq	#1,d6
	rts
;************************************
setup_hall_mem:
	bsr	letemfly_change
	movem.l	d0-a6,-(sp)
	move.l	hall_adresse(pc),a0
	move.l	echo_length(pc),d1
	move.l	d1,d6
	moveq	#HALLT,d0
	bsr	.write_mem

.loop:
	moveq	#HALL,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	and	#$7fff,d7

	cmp	#HALLU,d7
	bne.s	.no_up
	add.l	#256,d6
	cmp.l	maximum(pc),d6
	blo.s	.ok1
	sub.l	#256,d6
	bra.s	.loop
.ok1:
	move.l	hall_adresse(pc),a0
	move.l	d6,d1
	bsr	.write_mem
	bra.s	.loop
.no_up:
	cmp	#HALLD,d7
	bne.s	.no_down
	sub.l	#256,d6
	ifne	debug
	beq	.keine_null
	elseif
	beq	.keine_null
	endc
	bpl.s	.ok2
	add.l	#256,d6
	bra.s	.loop
.ok2:
	move.l	hall_adresse(pc),a0
	moveq	#HALLT,d0
	move.l	d6,d1
	ifne	debug
	bsr	.write_mem
	elseif
	bsr	.write_mem
	endc
	bra.s	.loop
.no_down:
	cmp	#HALLUU,d7
	bne.s	.no_dup
	add.l	#256*40,d6
	cmp.l	maximum(pc),d6
	blo.s	.ok3
	sub.l	#256*40,d6
	bra.s	.loop
.ok3:
	move.l	hall_adresse(pc),a0
	moveq	#HALLT,d0
	move.l	d6,d1
	bsr.s	.write_mem
	bra	.loop
.no_dup:
	cmp	#HALLDD,d7
	bne.s	.no_ddown
	sub.l	#256*40,d6
	beq.s	.keine_null
	bpl.s	.ok4
	add.l	#256*40,d6
	bra	.loop
.ok4:
	move.l	hall_adresse(pc),a0
	moveq	#HALLT,d0
	move.l	d6,d1
	bsr.s	.write_mem
	bra	.loop
.no_ddown:
	cmp	#HALLOK,d7
	beq.s	.ende
	move.l	echo_length(pc),d6
.ende:
	move.l	d6,echo_length
	movem.l	(sp)+,d0-a6
	bsr	letemfly_change
	rts
.keine_null:
	move.l	#256,d6
	move.l	hall_adresse(pc),a0
	move	d7,d0
	move.l	d6,d1
	bsr.s	.write_mem
	ifne	debug
	bra	.loop
	elseif
	bra	.loop
	endc
	
.write_mem:
	movem.l	d0-a6,-(sp)
	lea	HALLT*OBJECT(a0),a0
	move.l	ob_spec(a0),a0
	move.l	(a0),a0
	lea	(a0),a1
	moveq	#7,d0
.clr_loop:
	clr.b	(a1)+
	dbf	d0,.clr_loop

	move.l	echo_length(pc),d2
	bsr	copy_ldez
	movem.l	(sp)+,d0-a6
	rts
;************************************
neue_schwelle:
	bsr	letemfly_change
	movem.l	d0-a6,-(sp)
	move.b	schwelle(pc),d0
	sub.b	#offset,d0
	move.b	d0,akt_schwelle

	move.l	konfschw_adresse(pc),a0
	moveq	#EDITSCHW,d0
	move.b	akt_schwelle(pc),d1
	bsr	write_byte
.loop:
	moveq	#KONFSCHW,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	and	#$7fff,d7

	cmp	#SCHWUP,d7
	bne.s	.no_up
	addq.b	#1,akt_schwelle
	move.l	konfschw_adresse(pc),a0
	moveq	#EDITSCHW,d0
	move.b	akt_schwelle(pc),d1
	bsr	write_byte
	bra.s	.loop
.no_up:
	cmp	#SCHWDOWN,d7
	bne.s	.no_down
	subq.b	#1,akt_schwelle
	move.l	konfschw_adresse(pc),a0
	moveq	#EDITSCHW,d0
	move.b	akt_schwelle(pc),d1
	bsr	write_byte
	bra.s	.loop
.no_down:
	cmp	#SCHWDUP,d7
	bne.s	.no_dup
	add.b	#10,akt_schwelle
	move.l	konfschw_adresse(pc),a0
	moveq	#EDITSCHW,d0
	move.b	akt_schwelle(pc),d1
	bsr	write_byte
	bra.s	.loop
.no_dup:
	cmp	#SCHWDDOW,d7
	bne.s	.no_ddown
	sub.b	#10,akt_schwelle
	move.l	konfschw_adresse(pc),a0
	moveq	#EDITSCHW,d0
	move.b	akt_schwelle(pc),d1
	bsr	write_byte
	bra	.loop
.no_ddown:
	cmp	#SCHWOK,d7
	beq.s	.ende
	move.b	schwelle(pc),d0
	sub.b	#offset,d0
	move.b	d0,akt_schwelle
.ende:
	move.b	akt_schwelle(pc),d0
	add.b	#offset,d0
	move.b	d0,schwelle
	movem.l	(sp)+,d0-a6
	bsr	letemfly_change
	rts
;************************************
neue_frequenz:
	movem.l	d0-a6,-(sp)
	bsr	letemfly_change
	move.b	timer_a_data(pc),akt_timer_a_data
	move.l	konffreq_adresse(pc),a0
	bsr.s	write_frequenz
.loop:
	moveq	#KONFFREQ,d0
	bsr	form_do
	move	d7,d0
	clr.l	d1
	bsr	objc_change
	and	#$7fff,d7

	cmp	#FREQUP,d7
	bne.s	.no_up
	addq.b	#1,akt_timer_a_data
	bsr.s	write_frequenz
	bra.s	.loop
.no_up:
	cmp	#FREQDOWN,d7
	bne.s	.no_down
	subq.b	#1,akt_timer_a_data
	bsr.s	write_frequenz
	bra.s	.loop
.no_down:
	cmp	#FREQDUP,d7
	bne.s	.no_dup
	add.b	#10,akt_timer_a_data
	bsr.s	write_frequenz
	bra.s	.loop
.no_dup:
	cmp	#FREQDDOW,d7
	bne.s	.no_ddown
	sub.b	#10,akt_timer_a_data
	bsr.s	write_frequenz
	bra.s	.loop
.no_ddown:
	cmp	#FREQOK,d7
	beq.s	.ende
	move.b	timer_a_data(pc),akt_timer_a_data
.ende:
	move.b	akt_timer_a_data(pc),timer_a_data
	move.l	frequenz(pc),d0
	bsr	letemfly_change
	movem.l	(sp)+,d0-a6
	rts
;************************************
write_frequenz:
	movem.l	d0-d2/d5/d7/a0/a6,-(sp)
	lea	(a0),a6

	lea	EDITFREQ*OBJECT(a0),a0
	move.l	ob_spec(a0),a0
	move.l	(a0),a0
	lea	(a0),a1
	moveq	#5,d0
.loop:
	clr.b	(a1)+
	dbf	d0,.loop

	bsr.s	errechne_frequenz
	move.l	d2,d1
	move.l	d2,frequenz
	bsr	copy_ldez
	move.l	d2,d5

	lea	ZEIT*OBJECT(a6),a0
	move.l	ob_spec(a0),a0
	lea	(a0),a1
	moveq	#7,d0
.clr:	clr.b	(a1)+
	dbf	d0,.clr

	move.l	maximum(pc),d2
	bsr.s	write_time

	movem.l	(sp)+,d0-d2/d5/d7/a0/a6
	rts
;************************************
errechne_frequenz:
	move.l	d5,-(sp)
	clr.l	d5
	move.b	akt_timer_a_data(pc),d5
	move.l	#max_frequenz,d2
	bsr	long_div
	move.l	(sp)+,d5
	rts
;************************************
write_time:
;IN:	d5:	Frequency
;	d2:	Number of tics.
;OUT:	Time
	bsr	long_div
	move.l	d2,d7	;Ges.Sekunden.

	move.l	d7,d1
	divu	#3600,d1	;Ges.Sekunden/3600=Stunden.
	and.l	#$ffff,d1
	bsr.s	.copy_it	;Erwartet ein Byte...
	move.b	#':',(a0)+
	mulu	#3600,d1
	sub.l	d1,d7	;Ges.Sek.-(Stunden*3600)=restl.Sekunden.

	move.l	d7,d1
	divu	#60,d1
	and.l	#$ffff,d1
	bsr.s	.copy_it
	move.b	#':',(a0)+
	mulu	#60,d1
	sub.l	d1,d7

	move.l	d7,d1
	and.l	#$ffff,d1
	bsr.s	.copy_it
	rts
.copy_it:
	movem.l	d1-d4,-(sp)
	clr.l	d2
	tst.b	d1
	beq.s	.null
	move.b	d1,d4
	moveq	#10,d3
.loop1:
	clr.l	d2
.loop2:
	cmp.b	d3,d4
	bmi.s	.is_lower
	addq.b	#1,d2
	sub.b	d3,d4
	bne.s	.loop2
.is_lower:
	add.b	#'0',d2
	move.b	d2,(a0)+
.is_zero:
	divu	#10,d3
	bne.s	.loop1
.ende:
	movem.l	(sp)+,d1-d4
	rts
.null:
	move.b	#'0',(a0)+
	move.b	#'0',(a0)+
	bra.s	.ende
;************************************
write_byte:
;IN:	d0:	Object#
;	d1:	Value
;	a0:	Object address
	movem.l	d0-d1/a0,-(sp)
	mulu	#OBJECT,d0
	lea	(a0,d0.l),a0
	move.l	ob_spec(a0),a0
	move.l	(a0),a0
	clr.b	(a0)
	clr.b	1(a0)
	clr.b	2(a0)
	bsr	copy_ldez
	movem.l	(sp)+,d0-d1/a0
	rts
;************************************
;*******AES**************************
;************************************
form_alert:
	form_alert	d0,a0
	rts
;************************************
menu_tnormal:
	menu_tnormal	message_puffer+6(pc),#1,menu_adresse(pc)
	rts
;************************************
menu_icheck:
	menu_icheck	d0,d1,a0
	rts
;************************************
menu_ienable:
	menu_ienable	d0,d1,a0
	rts
;************************************
objc_draw:
	objc_draw	d0,#MAX_DEPTH,x(pc),y(pc),w(pc),h(pc),a0
	rts
;************************************
form_center:
	form_center	a0
	movem	d0-d3,-(sp)
	movem	int_out+2(pc),d0-d3
	movem	d0-d3,x
	movem	(sp)+,d0-d3
	rts
;************************************
form_do:
	bsr.s	form_center	;Objekt zentrieren.

	move.l	d0,-(sp)
	moveq	#FMD_START,d0	;Bildschirmausschnitt retten.
	bsr.s	form_dial
	move.l	(sp),d0

	bsr	objc_draw	;Objekt malen.
	form_do	#0,a0	;Aktion ausf�hren...
	move	int_out(pc),d7	;Objektnummer wird in int_out geliefert.

	moveq	#FMD_FINISH,d0	;Bildschirm wieder herstellen.
	bsr.s	form_dial
	move.l	(sp)+,d0
	rts
;************************************
form_dial:
	form_dial	d0,x(pc),y(pc),w(pc),h(pc),x(pc),y(pc),w(pc),h(pc)
	rts
;************************************
objc_change:
	objc_change	d0,x(pc),y(pc),w(pc),h(pc),d1,#0,a0
	rts
;************************************
rsrc_gaddr:
	rsrc_gaddr	d0,d1,a0
	tst	int_out
	rts
;************************************
evnt_multi:
	evnt_multi	#%11,#1+256,#%11,#0
	rts
;************************************
wind_update:
	wind_update	d0
	rts
;************************************
menu_bar_off:
	clr.l	d0
	bra.s	menu_bar
;************************************
menu_bar_on:
	moveq	#1,d0
;************************************
menu_bar:
	menu_bar	d0,menu_adresse(pc)
	rts
;************************************
stop_aes:
	move.l	d0,-(sp)
	move	#M_OFF,d0
	bsr.s	graf_mouse
	moveq	#update_off,d0
	bsr.s	wind_update
	move.l	(sp)+,d0
	bsr	maus_aus
	rts
;************************************
start_aes:
	bsr	maus_an
	move.l	d0,-(sp)
	moveq	#update_on,d0
	bsr.s	wind_update
	move	#M_ON,d0
	bsr.s	graf_mouse
	move.l	(sp)+,d0
	rts
;************************************
graf_mouse:
	graf_mouse	d0
	rts
;************************************
;******VDI***************************
;************************************
v_pline:
	move	d0,-(sp)
	movem	d0-d3,ptsin
	v_pline	2
	move	(sp)+,d0
	rts
;************************************
v_string:
	movem.l	d1/a1,-(sp)
.aussen_loop
	lea	vdi_buffer(pc),a1
	moveq	#126,d0
.loop
	move.b	(a0)+,(a1)+
	seq	d1
	beq.s	.letztes_zeichen
	dbf	d0,.loop
.letztes_zeichen
	bsr.s	v_curtext
	tst.b	d1
	beq.s	.aussen_loop
.ende
	movem.l	(sp)+,d1/a1
	rts
;************************************
v_curtext:
	v_curtext	vdi_buffer(pc)
	rts
;************************************
v_cout:
	move.l	a0,-(sp)
	lea	vdi_buffer(pc),a0
	clr	(a0)
	move.b	d0,(a0)
	bsr.s	v_curtext
	move.l	(sp)+,a0
	rts
;************************************
v_curhome:
	v_curhome
	rts
;************************************
v_eeos:
	v_eeos
	rts
;************************************
clr_scn:
	bsr.s	v_curhome
	bsr.s	v_eeos
	rts
;************************************
;********Eigener Brei****************
;************************************
arrow:
	move.l	d0,-(sp)
	moveq	#ARROW,d0
_arrow:
	bsr	graf_mouse
	move.l	(sp)+,d0
	rts
busybee:
	move.l	d0,-(sp)
	moveq	#BUSYBEE,d0
	bra.s	_arrow
;************************************
letemfly_change:
	movem.l	d7/a0,-(sp)
	move.l	#'LTMF',d7
	bsr	find_cookie
	tst.l	d7
	beq.s	.ende
	move.l	d7,a0
	move	2(a0),d0
	bchg	#0,d0
	move	d0,2(a0)
.ende:	movem.l	(sp)+,d7/a0
	rts
;************************************
init_resources:
	movem.l	d1/a0-a3,-(sp)
	rsrc_load	#resource_name
	moveq.b	#-1,d0
	tst	int_out
	beq	.rsrc_load_error

	moveq	#R_TREE,d0
	moveq	#MENUE,d1
	bsr	rsrc_gaddr
	move.l	a0,menu_adresse

	moveq	#R_TREE,d0
	moveq	#KONFFREQ,d1
	bsr	rsrc_gaddr
	move.l	a0,konffreq_adresse

	moveq	#R_TREE,d0
	moveq	#KONFSCHW,d1
	bsr	rsrc_gaddr
	move.l	a0,konfschw_adresse

	moveq	#R_TREE,d0
	moveq	#DPEAK,d1
	bsr	rsrc_gaddr
	move.l	a0,peak_adresse

	moveq	#R_TREE,d0
	moveq	#HALL,d1
	bsr	rsrc_gaddr
	move.l	a0,hall_adresse

	moveq	#R_TREE,d0
	moveq	#INFO1,d1
	bsr	rsrc_gaddr
	move.l	a0,info_adresse

	moveq	#R_TREE,d0
	moveq	#INFO2,d1
	bsr	rsrc_gaddr
	move.l	a0,info_adresse+4

	lea	jmp_title(pc),a0
	lea	key_title(pc),a3
	lea	(a0),a1
	lea	(a3),a4
	lea	return(pc),a2
	moveq	#127,d0
.loop:
	move.l	a2,(a1)+
	move.l	a2,(a4)+
	dbf	d0,.loop
;Mag-Sound...
	move.l	#ueber_mag_sound,UEBER*4(a0)
	move.l	#ueber_mag_sound,SCAN_HELP*4(a3)
;File...
	move.l	#load_iff_8svx,LOAD8*4(a0)
	move.l	#load_iff_8svx,SCAN_G*4(a3)
	move.l	#load_amiga,LOADAM*4(a0)
	move.l	#load_amiga,SCAN_R*4(a3)
	move.l	#load_atari,LOADAT*4(a0)
	move.l	#load_atari,SCAN_L*4(a3)
	move.l	#save_iff_8svx,SAVE8*4(a0)
	move.l	#save_iff_8svx,SCAN_P*4(a3)
	move.l	#save_amiga,SAVEAM*4(a0)
	move.l	#save_amiga,SCAN_W*4(a3)
	move.l	#save_atari,SAVEAT*4(a0)
	move.l	#save_atari,SCAN_S*4(a3)
	move.l	#merge,MERGE*4(a0)
	move.l	#merge,SCAN_M*4(a3)
	move.l	#daten_senden,SEND*4(a0)
	move.l	#daten_empfangen,RECEIVE*4(a0)
	move.l	#quit,QUIT*4(a0)
	move.l	#quit,SCAN_Q*4(a3)
;Sample bearbeiten...
	move.l	#make_hall,MAKEHALL*4(a0)
	move.l	#make_hall,SCAN_E*4(a3)
	move.l	#volume,VOLUME*4(a0)
	move.l	#volume,SCAN_V*4(a3)
	move.l	#drehen,DREHEN*4(a0)
	move.l	#drehen,SCAN_T*4(a3)
	move.l	#sample,EINLESEN*4(a0)
	move.l	#sample,SCAN_I*4(a3)
	move.l	#loeschen,LOESCHEN*4(a0)
	move.l	#loeschen,SCAN_C*4(a3)
;Ausgabe...
	move.l	#play_monitor,MONITOR*4(a0)
	move.l	#play_monitor,SCAN_X*4(a3)
	move.l	#play_monitor_rueckw,RMONITOR*4(a0)
	move.l	#play_monitor_rueckw,SCAN_B*4(a3)
	move.l	#play_parallel,PARALLEL*4(a0)
	move.l	#play_parallel,SCAN_J*4(a3)
	move.l	#play_hall_centronics,CENTHALL*4(a0)
	move.l	#play_hall_centronics,SCAN_K*4(a3)
	move.l	#grafisch_darstellen,GRAFIK*4(a0)
	move.l	#grafisch_darstellen,SCAN_Z*4(a3)
	move.l	#fourier_analyse,FOURIER*4(a0)
	move.l	#fourier_analyse,SCAN_Y*4(a3)
	move.l	#dfilter,EFILTER*4(a0)
	move.l	#dfilter,SCAN_D*4(a3)
;Optionen
	move.l	#aussteuern,AUSSTEUE*4(a0)
	move.l	#aussteuern,SCAN_A*4(a3)
	move.l	#neue_frequenz,FREQUENZ*4(a0)
	move.l	#neue_frequenz,SCAN_F*4(a3)
	move.l	#neue_schwelle,SCHWELLE*4(a0)
	move.l	#neue_schwelle,SCAN_U*4(a3)
	move.l	#setup_hall_mem,HALLMEM*4(a0)
	move.l	#setup_hall_mem,SCAN_N*4(a3)
	move.l	#peak_suchen,PEAK*4(a0)
	move.l	#peak_suchen,SCAN_H*4(a3)

	clr.b	d0
.rsrc_load_error:
	movem.l	(sp)+,d1/a0-a3
	rts
;************************************
clip_on:
	movem.l	d0-d2,-(sp)
	moveq	#1,d0
clip_help:
	movem	max_x(pc),d1-d2
	vs_clip	d0,#0,#0,d1,d2
	movem.l	(sp)+,d0-d2
	rts
clip_off:
	movem.l	d0-d2,-(sp)
	clr	d0
	bra.s	clip_help
;************************************	
reserve_screen:
	movem.l	d0-d4,-(sp)
	moveq	#FMD_START,d0
screen_help:
	clr	d1
	move	d1,d2
	movem	max_w(pc),d3-d4
	movem	d1-d4,x
	bsr	form_dial
	movem.l	(sp)+,d0-d4
	rts
;************************************
free_screen:
	movem.l	d0-d4,-(sp)
	moveq	#FMD_FINISH,d0
	bra.s	screen_help
;************************************
ciao_gem:
	tst	d0
	beq.s	.ciao
	bsr	start_aes
	bsr.s	free_screen
	bsr	menu_bar_on
	rts
.ciao:
	bsr	menu_bar_off
	bsr.s	reserve_screen
	bsr	stop_aes
	bsr	clr_scn
	bsr.s	dosound
	rts
;************************************
zeige_abbruch_box:
	move.l	a0,-(sp)
	moveq	#2,d0
	lea	abbruch(pc),a0
	bsr	form_alert
	move.l	(sp)+,a0
	cmp	#FA_MIDDLE,int_out
	rts
;************************************
maus_aus:
	move.l	a0,-(sp)
	clr.l	d0
	lea	maus_report_aus(pc),a0
	bsr.s	kybd_comm
	move.l	(sp)+,a0
	rts
;************************************
maus_an:
	move.l	a0,-(sp)
	clr.l	d0
	lea	maus_report_an(pc),a0
	bsr.s	kybd_comm
	move.l	(sp)+,a0
	rts
;************************************
calc_screenoffset:
	move	max_h(pc),d0
	lsr	#1,d0
	sub	#offset,d0
	mulu	#80,d0
	add.l	d0,a6
	rts
;************************************
;*****GEMDose************************
;************************************
malloc:
	Malloc	d0
	rts
;************************************
;*****Bios&XBios*********************
;************************************
dosound:
	Dosound	sounds(pc)
	rts
;************************************
kybd_comm:
	Ikbdws	(a0),d0
	rts
;************************************
logbase:
	Logbase
	move.l	d0,a6
	rts
;************************************
supexec:
	Supexec	(a6)
	rts
;************************************
;*******'Harte' Routinen*************
;************************************
farben:
	bset	#no_break_flag,flags
	move.l	d0,-(sp)
	moveq	#100,d0
.farbe:
	not	(color0).w
	not	(color0).w
	dbf	d0,.farbe
	move.l	(sp)+,d0
	bclr	#no_break_flag,flags
	rts
;************************************
klick_off:
	move.b	(conterm).w,klick
	and.b	#%1010,(conterm).w
	rts
;************************************
klick_on:
	move.b	klick(pc),(conterm).w
	rts
;************************************
install_timer:
	move.b	timer_a_data(pc),(tadr).w
	move.b	#1,(tacr).w
	rts
;************************************
new_kbd:
	move.b	(keybd).w,vdi_buffer	;Taste abholen.
	btst	#no_break_flag,flags(pc)
	bne.s	.weiter
	cmp.b	#1+128,vdi_buffer	;ESC losgelassen?
	bne.s	.weiter	;Nein, dann Ende.
	clr.b	vdi_buffer
	move.l	return_kbd(pc),2(sp)	;Doch, dann wird abgebrochen.
.weiter:
	bclr	#PORT_I4,(isrb).w	;Interrupt l�schen.
	rte	;Zur�ck in die Zukunft.
;************************************
my_interrupt_on:
	or	#$700,sr
	move.b	(imra).w,saved_mask_a

	ifeq	debug
	clr.b	(imra).w
	endc

	move.b	(imrb).w,saved_mask_b
	ifeq	debug
	clr.b	(imrb).w
	endc

	ifeq	debug
	move.l	$118.w,old_kbd
	move.l	#new_kbd,$118.w
	endc
	bset	#TIMER_A,(iera).w
	bclr	#PORT_I4,(iprb).w
	bset	#PORT_I4,(imrb).w
	move	#$2500,sr
	rts
;************************************
my_interrupt_off:
play_int_ende:
	or	#$700,sr
	ifeq	debug
	clr.b	(imra).w
	clr.b	(imrb).w
	endc
	bclr	#TIMER_A,(iera).w
	bclr	#TIMER_A,(ipra).w
	bclr	#TIMER_A,(isra).w

	ifeq	debug
	move.l	old_kbd(pc),$118.w
	endc

	move.b	saved_mask_a(pc),(imra).w
	move.b	saved_mask_b(pc),(imrb).w
	move	#$2300,sr		
	rts
;************************************
sample_int_ende:
	bsr.s	my_interrupt_off
.sample_weiter:
	move.l	maximum(pc),d0
	sub.l	d1,d0
	move.l	d0,scratch_length
	rts
;************************************
;*********Filedings******************
;************************************
file_input:
	lea	filename(pc),a0
	lea	pfad(pc),a1
	include	sub\fsel_inp.s
;************************************
fopen:
	Fopen	d0,(a0)
	rts
;************************************
fcreate:
	Fcreate	d0,(a0)
	rts
;************************************
fwrite:
	Fwrite	(a1),d0,d1
	rts
;************************************
fread:
	Fread	(a1),d0,d1
	rts
;************************************
fclose:
	Fclose	handle(pc)
	rts
;************************************
fsfirst:
	Fsfirst	d0,(a0)
	rts
;************************************
save_file:	;leuchtet ein, gell?
	btst	#noch_nicht_saved,flags(pc)
	bne.s	.weiter
	moveq	#-1,d0
	bra.s	.nicht_abspeichern
.weiter:
	lea	save_it(pc),a2
	bsr	file_input
	tst	d0
	bne.s	.weiter2
	moveq	#-1,d0
	bra.s	.nicht_abspeichern
.weiter2:
	bsr	busybee

	clr.l	d0
	bsr	fcreate
      	move	d0,handle
	bmi	file_error
	move	d0,d1
	move.l	length(pc),d0
	move.l	root_start(pc),a1
	bsr	fwrite
	move.l	d0,d1
	bsr.s	fclose
	bclr	#noch_nicht_saved,flags
	clr.l	d0
.nicht_abspeichern:
	bsr	arrow
	rts
;************************************
load_file:	;fragt nach einem File-Namen,
	lea	load_it(pc),a2
	bsr	file_input
	tst	d0
	bne.s	.weiter

	moveq	#-1,d0
	bra	.get_file_in_memory
.weiter:
	bsr	busybee
	clr.l	d0
	bsr	fsfirst
	tst	d0
	bmi	file_error

	Fgetdta
	move.l	d0,a2
	move.l	d_length(a2),file_size
	move.l	d_length(a2),d0

	btst	#merge_flag,flags(pc)
	beq.s	.normal_load

.merge:
	add.l	length(pc),d0
	cmp.l	maximum(pc),d0
	bhi.s	.zu_gross
	bra.s	.ok
.normal_load:
	move.l	root_start(pc),next_start
.ok:
	clr.l	d0
	bsr	fopen
	move	d0,handle
	bmi.s	file_error

	move.l	next_start(pc),a1
	move.l	file_size(pc),d0
	move	handle(pc),d1
	bsr	fread
	cmp.l	file_size(pc),d0
	bne.s	file_error
	bsr	fclose

	bset	#memory_dirty,flags
	bclr	#noch_nicht_saved,flags

	btst	#merge_flag,flags(pc)
	bne.s	.1
	clr.l	length
.1:	move.l	file_size(pc),d0
	add.l	d0,length
	clr.l	d0

.get_file_in_memory:
	bsr	arrow
	rts

.zu_gross:
	bsr	arrow
	moveq	#1,d0
	lea	file_max(pc),a0
	bsr	form_alert
	moveq	#-1,d0
	rts
;************************************
;***************ERRORS***************
file_error:
	bsr	arrow
	bsr.s	form_error
	bsr	fclose
	btst	#memory_dirty,flags(pc)
	beq.s	.weiter
	btst	#amiga_sample,flags(pc)
	beq.s	.weiter
	bsr	convert
.weiter:
	moveq	#-1,d0
return:
	rts
;**********************************
;**********************************
	include	sub\form_err.s
	include	include\call_gem
	include	sub\asc_hex.s
	include	sub\copyldez.s
	include	sub\long_div.s
	include	sub\f_cookie.s
;**********************************
;****************DATA**************
	data
iff_header:	dc.l	'FORM'
form_length:	dc.l	0
iff_8svx:	dc.l	'8SVX'

auth:	dc.l	'AUTH'
	dc.l	vhdr-auth_start
auth_start:	dc.l	'MAG-','SND '

vhdr:	dc.l	'VHDR'
	dc.l	body-vhdr_start
vhdr_start:	dc.l	0,0,0
iff_frequenz:	dc.w	0
	dc.b	1,0
	dc.l	-1
body:	dc.l	'BODY'
body_length:	dc.l	0
iff_header_ende:

sounds:	dc.b	0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,$7f,8,0,9,0
	dc.b	10,0,11,$ff,12,$ff,13,0,$82,0

maus_report_aus:	dc.b	$12
maus_report_an:	dc.b	8

tabelle:	include	'tab1.s'
sinus:	dc.w	0,1,2,3,4,5,6,7,8,9,$a,$b,$c,$e,$f
	dc.w	$10,$11,$12,$13,$14,$15,$16,$17
	dc.w	$18,$19,$1a,$1b,$1c,$1d,$1e,$1f
	dc.w	$20,$21,$22,$23,$24,$25,$26,$27
	dc.w	$28,$29,$2a,$2b,$2c,$2d,$2e,$2f
	dc.w	$30,$31,$32,$33,$34,$35,$36,$37
	dc.w	$38,$39,$3a,$3a,$3b,$3c,$3d,$3e
	dc.w	$3f,$40,$41,$42,$43,$44,$45,$46
	dc.w	$47,$48,$49,$4a,$4b,$4c,$4d,$4e
	dc.w	$4f,$50,$51,$52,$53,$53,$54,$55
	dc.w	$56,$57,$58,$59,$5a,$5b,$5c,$5d
	dc.w	$5e,$4f,$60,$61,$61,$62,$63,$64
	dc.w	$65,$66,$67,$68,$69,$6a,$6b,$6c
	dc.w	$6c,$6d,$6e,$6f,$70,$71,$72,$73
	dc.w	$74,$75,$75,$76,$77,$78,$79,$7a
	dc.w	$7b,$7c,$7c,$7d,$7e,$7f,$80,$81
	dc.w	$82,$83,$83,$84,$85,$86,$87,$88
	dc.w	$89,$89,$8a,$8b,$8c,$8d,$8e,$8e
	dc.w	$8f,$90,$91,$92,$93,$93,$94,$95
	dc.w	$96,$97,$97,$98,$99,$9a,$9b,$9b
	dc.w	$9c,$9d,$9e,$9f,$9f,$a0,$a1,$a2
	dc.w	$a2,$a3,$a4,$a5,$a6,$a6,$a7,$a8
	dc.w	$a9,$a9,$aa,$ab,$ac,$ac,$ad,$ae
	dc.w	$ae,$af,$b0,$b1,$b1,$b2,$b3,$b4
	dc.w	$b4,$b5,$b6,$b6,$b7,$b8,$b8,$b9
	dc.w	$ba,$bb,$bb,$bc,$bd,$bd,$be,$bf
	dc.w	$bf,$c0,$c1,$c1,$c2,$c3,$c3,$c4
	dc.w	$c4,$c5,$c6,$c6,$c7,$c8,$c8,$c9
	dc.w	$ca,$ca,$cb,$cb,$cc,$cd,$cd,$ce
	dc.w	$ce,$cf,$d0,$d0,$d1,$d1,$d2,$d2
	dc.w	$d3,$d4,$d4,$d5,$d5,$d6,$d6,$d7
	dc.w	$d7,$d8,$d8,$d9,$da,$da,$db,$db
	dc.w	$dc,$dc,$dd,$dd,$de,$de,$df,$df
	dc.w	$e0,$e0,$e1,$e1,$e2,$e2,$e2,$e3
	dc.w	$e3,$e4,$e4,$e5,$e5,$e6,$e6,$e7
	dc.w	$e7,$e7,$e8,$e8,$e9,$e9,$ea,$ea
	dc.w	$ea,$eb,$eb,$ec,$ec,$ec,$ed,$ed
	dc.w	$ed,$ee,$ee,$ef,$ef,$ef,$f0,$f0
	dc.w	$f0,$f1,$f1,$f1,$f2,$f2,$f2,$f3
	dc.w	$f3,$f3,$f4,$f4,$f4,$f4,$f5,$f5
	dc.w	$f5,$f6,$f6,$f6,$f6,$f7,$f7,$f7
	dc.w	$f8,$f8,$f8,$f8,$f8,$f9,$f9,$f9
	dc.w	$f9,$fa,$fa,$fa,$fa,$fb,$fb,$fb
	dc.w	$fb,$fb,$fb,$fc,$fc,$fc,$fc,$fc
	dc.w	$fd,$fd,$fd,$fd,$fd,$fd,$fd,$fe
	dc.w	$fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
	dc.w	$fe,$ff,$ff,$ff,$ff,$ff,$ff,$ff
	dc.w	$ff,$ff,$100,$100,$100,$100,$100,$100
	dc.w	$100,$100,$100,$100,$100,$100,$100,$100
	dc.w	$100,$100,$100
bitrev_tab:
bitrev_t:
bit	set	0
	REPT	anzahl
	dc.w	bit
bit	set	bit+1
	ENDR
	IFNE	TEST
sinus_bin:	incbin	sinus.bin
	ENDC
;****************FILESTRINGS***********
load_it:	dc.b	'Datei einladen:',0
save_it:	dc.b	'Datei abspeichern:',0
resource_name:	dc.b	'MAG_SND.RSC',0

;****************ALARMBOXEN************
fehler_iff_laden:	dc.b	'[3][Fehler beim Einlesen|der IFF-8SVX-Datei.][Abbruch]',0
a_iff_frequenz:	dc.b	'[3][Das IFF-Verfahren erlaubt es nur,|Frequenzen bis zu 65535 Hz|'
	dc.b	'abzuspeichern.][Abbruch]',0
a_neue_frequenz:	dc.b	'[2][Frequenz des Samples|ver�ndern.][Doppelte|Halbe|Abbruch]',0
lauter:	dc.b	'[2][Lautst�rke des Samples|ver�ndern.][Lauter|Leiser|Abbruch]',0
only_hires:	dc.b	'[1][Mag-Sound l�uft (leider)|nur in der Aufl�sung|mit'
	dc.b	'640*400 Punkten][ OK ]',0
file_max:	dc.b	'[3][Das File ist zu gross.|Breche ab.][Abbruch]',0
abbruch:	dc.b	"[1][ESCAPE kehrt zum|Hauptmen� zur�ck.][ OK |Abbruch]",0
noch_nicht_abgespeichert:	dc.b	'[3][Es befindet sich ein Sample|'
	dc.b	'im Speicher, das noch|nicht abgespeichert wurde.]'
	dc.b	'[ OK |Abbruch]',0
keine_rsrc_datei:	dc.b	'[3][Keine RSC-Datei gefunden!|Das Programm wird|abgebrochen.][Abbruch]',0
;****************TEXTE******************
fourier_str1:	dc.b	'Fourieranalyse �ber 512 Me�punkte:',$d,$a
	dc.b	'Es werden 256 Koeffizienten ausgegeben, da Symmetrie vorherrscht.',$d,$a
	dc.b	'Bitte h�ufiger ESCAPE dr�cken, da innerhalb',$d,$a
	dc.b	'der Koeffizientenberechnung alle Interrupts ignoriert werden.',0
aus_str:	dc.b	'Aussteuerung:',$d,$a
	dc.b	'Sampler so justieren, da� m�glichst wenige horizontale Linien (als Zeichen',$d,$a
	dc.b	'f�r �bersteuerung) entstehen. SPACE stoppt die Grafikausgabe, so da� die',$d,$a
	dc.b	'Soundqualit�t deutlich besser wird. (Fast so gut, wie bei der Aufnahme.)',0
peak_str:	dc.b	'Es wird die lauteste Stelle ab Start der Messung gesucht.',$d,$a
	dc.b	'Bei einem neuen Spitzenwert der eingelesenen Daten blinkt der',$d,$a
	dc.b	'Bildschirm kurz auf.',0

	copyright
;*****************BSS**************
	bss
fft_tab:	ds.l	anzahl*2
y_save:	ds.l 160
jmp_title:	ds.l	128
key_title:	ds.l	128
vdi_buffer:	ds.l	32
filename:	ds.l 20
pfad:	ds.l	20
gemdos_pfad:	ds.l	20
message_puffer:	ds.l	4
info_adresse:	ds.l	2
echo_length:	ds.l	1
peak_time:	ds.l	1
length:	ds.l 1
rest:	ds.l 1
int_position:	ds.l 1
frequenz:	ds.l	1
root_start:	ds.l 1
scratch_length:	ds.l	1
next_start:	ds.l 1
maximum:	ds.l 1
return_kbd:	ds.l	1
old_kbd:	ds.l	1
file_size:	ds.l 1
mem_free:	ds.l 1
next_im:	ds.l	1
next_be:	ds.l	1
old_be:	ds.l	1
menu_adresse:	ds.l	1
konffreq_adresse:	ds.l	1
konfschw_adresse:	ds.l	1
peak_adresse:	ds.l	1
hall_adresse:	ds.l	1
jmp_fft:	ds.l	1
new:	ds.w	1
old:	ds.w	1
wr:	ds.w	1
wi:	ds.w	1
tr:	ds.w	1
ti:	ds.w	1
handle:	ds.w	1
x:	ds.w	1
y:	ds.w	1
w:	ds.w	1
h:	ds.w	1
max_x:	ds.w	1
max_y:	ds.w	1
max_w:	ds.w	1
max_h:	ds.w	1
oben:	ds.w	1
unten:	ds.w	1
dezimal_buffer:	ds.b	5
vor:	ds.b	1
peak:	ds.b	1
flags:	ds.b	1
akt_timer_a_data:	ds.b	1
timer_a_data:	ds.b	1
akt_schwelle:	ds.b	1
schwelle:	ds.b	1
saved_mask_a:	ds.b	1
saved_mask_b:	ds.b	1
klick:	ds.b	1
letemfly:	ds.b	1
	end
